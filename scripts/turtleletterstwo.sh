#!/usr/bin/bash

# Clear Screen
rosservice call clear

# Move Turtle to Bottom Left 
rosservice call /turtle1/set_pen 255 0 0 1 1
rosservice call /turtle1/teleport_absolute 1 5 0
rosservice call /turtle1/set_pen 255 0 0 3 0
# First letter (A)
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 4.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 0.0, 0.0]' '[0.0, 0.0, 1.571]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, -3.13]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[4.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[-2.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, -2.546, 0.0]' '[0.0, 0.0, 0.0]'

# Move Turtle to Middle and change color 
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 9 5 0
rosservice call /turtle1/set_pen 0 255 0 3 0

# Second Letter (a) THIS IS LOWERCASE SO IT'S LEGAL
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, 3.0, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, -1.75, 0.0]' '[0.0, 0.0, 0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist -- '[0.0, -7.0, 0.0]' '[0.0, 0.0, -6.1]'

# Move the turtle away
rosservice call /turtle1/set_pen 0 0 0 0 1
rosservice call /turtle1/teleport_absolute 9 9 0
rosservice call /turtle1/set_pen 0 255 0 3 0



